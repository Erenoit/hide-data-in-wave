mod cli;
mod wave;

use std::{
    fs::File,
    io::{Read, Write},
    path::PathBuf,
};

use clap::Parser;

use crate::{
    cli::{Cmd, SubCmd},
    wave::Wave,
};

const BLOCK_SIZE: usize = 2;
const SIZE_BITS: usize = 32;

fn main() {
    match Cmd::parse().sub_cmd {
        SubCmd::Hide {
            wave_file,
            file_to_hide,
            output,
        } => hide(wave_file, file_to_hide, output),
        SubCmd::Recover { wave_file, output } => recover(wave_file, output),
    }
}

fn hide(wave_file: PathBuf, file_to_hide: PathBuf, output: PathBuf) {
    let mut serialized_file = {
        let mut read_buffer: Vec<u8> = Vec::new();

        File::open(wave_file)
            .unwrap()
            .read_to_end(&mut read_buffer)
            .unwrap();

        assert!(!read_buffer.is_empty());

        Wave::serialize(&read_buffer)
    };

    let data_to_hide = {
        let mut read_buf: Vec<u8> = Vec::new();

        File::open(file_to_hide)
            .unwrap()
            .read_to_end(&mut read_buf)
            .unwrap();

        read_buf
    };

    assert!(BLOCK_SIZE as u16 <= serialized_file.fmt_subchunk.bits_per_sample);
    assert!(
        SIZE_BITS + data_to_hide.len() * 8 <= serialized_file.data_subchunk.data.len() * BLOCK_SIZE
    );

    let write_buf = &mut serialized_file.data_subchunk.data;
    let mut cursor = 0;

    // Write size of the data as 32 bit
    let mut data_size = data_to_hide.len();
    for _ in 0 .. SIZE_BITS / BLOCK_SIZE {
        write_buf[cursor] &= 0xFFFC;
        write_buf[cursor] |= (data_size & 0x00000003) as u16;
        data_size >>= BLOCK_SIZE;
        cursor += 1;
    }

    for mut byte in data_to_hide {
        for _ in 0 .. 8 / BLOCK_SIZE {
            write_buf[cursor] &= 0xFFFC;
            write_buf[cursor] |= (byte & 0x03) as u16;
            byte >>= BLOCK_SIZE;
            cursor += 1;
        }
    }

    File::create(output)
        .unwrap()
        .write_all(&serialized_file.deserialize())
        .unwrap();
}

fn recover(wave_file: PathBuf, output: PathBuf) {
    let serialized_file = {
        let mut read_buffer: Vec<u8> = Vec::new();

        File::open(wave_file)
            .unwrap()
            .read_to_end(&mut read_buffer)
            .unwrap();

        assert!(!read_buffer.is_empty());

        Wave::serialize(&read_buffer)
    };

    let read_buf = &serialized_file.data_subchunk.data;
    let mut cursor = 0;
    let mut data_size: u32 = 0;

    for _ in 0 .. SIZE_BITS / BLOCK_SIZE {
        data_size |= (read_buf[cursor] & 0x0003) as u32;
        data_size = data_size.rotate_right(BLOCK_SIZE as u32);
        cursor += 1;
    }

    let mut data_buf = Vec::<u8>::with_capacity(data_size as usize);

    for _ in 0 .. data_size {
        let mut byte = 0;

        for _ in 0 .. 8 / BLOCK_SIZE {
            byte |= (read_buf[cursor] & 0x0003) as u8;
            byte = byte.rotate_right(BLOCK_SIZE as u32);
            cursor += 1;
        }

        data_buf.push(byte);
    }

    File::create(output).unwrap().write_all(&data_buf).unwrap();
}

#[cfg(test)]
mod tests {
    use std::{
        fs::{self, File},
        io::Read,
        path::PathBuf,
    };

    use super::{hide, recover};

    #[test]
    fn hide_recover_corruption() {
        hide(
            PathBuf::from("./test_file.wav"),
            PathBuf::from("./test_image.jpg"),
            PathBuf::from("./test_file_w_data.wav"),
        );
        recover(
            PathBuf::from("./test_file_w_data.wav"),
            PathBuf::from("./test_image_recovered.jpg"),
        );

        let original = {
            let mut read_buffer: Vec<u8> = Vec::new();

            File::open("test_image.jpg")
                .unwrap()
                .read_to_end(&mut read_buffer)
                .unwrap();

            read_buffer
        };
        let recovered = {
            let mut read_buffer: Vec<u8> = Vec::new();

            File::open("test_image_recovered.jpg")
                .unwrap()
                .read_to_end(&mut read_buffer)
                .unwrap();

            read_buffer
        };

        assert_eq!(original, recovered);

        // Clean
        fs::remove_file("./test_file_w_data.wav").unwrap();
        fs::remove_file("./test_image_recovered.jpg").unwrap();
    }
}
