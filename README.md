# Hide Data in WAVE

This is just a proof of concept for hiding data inside other files. Using in production in any way is not recommended. Hiding data inside WAVE file *does not* mean your data is absolutely safe. If someone finds a way to extract your data they can read it normally. This program also *does not* encrypt your data.

## Quick Start

```sh
$ cargo build --release
$ ./target/release/hide_data_into_wave -h
```
