use std::{io::Cursor, mem::size_of};

use byteorder::{BigEndian, LittleEndian, ReadBytesExt, WriteBytesExt};

pub struct Wave {
    pub chunk_descriptor: ChunkDescriptor,
    pub fmt_subchunk:     FmtSubchunk,
    pub data_subchunk:    DataSubchunk,
}

impl Wave {
    pub fn serialize(data: &[u8]) -> Self {
        let s = Self {
            chunk_descriptor: ChunkDescriptor::serialize(&data[.. size_of::<ChunkDescriptor>()]),
            fmt_subchunk:     FmtSubchunk::serialize(
                &data[size_of::<ChunkDescriptor>()
                    .. size_of::<ChunkDescriptor>() + size_of::<FmtSubchunk>()],
            ),
            data_subchunk:    DataSubchunk::serialize(
                &data[size_of::<ChunkDescriptor>() + size_of::<FmtSubchunk>() ..],
            ),
        };

        s.check();

        s
    }

    pub fn deserialize(&self) -> Vec<u8> {
        self.check();
        self.chunk_descriptor.check();
        self.fmt_subchunk.check();
        self.data_subchunk.check();

        let mut raw = Vec::with_capacity(self.chunk_descriptor.chunk_size as usize);

        self.chunk_descriptor.deserialize(&mut raw);
        self.fmt_subchunk.deserialize(&mut raw);
        self.data_subchunk.deserialize(&mut raw);

        raw
    }

    fn check(&self) {
        assert_eq!(
            self.chunk_descriptor.chunk_size,
            36 + self.data_subchunk.subchunk_size
        );
    }
}

pub struct ChunkDescriptor {
    pub chunk_id:   u32, // big
    pub chunk_size: u32, // little
    pub format:     u32, // big
}

impl ChunkDescriptor {
    fn serialize(data: &[u8]) -> Self {
        let mut read_buf = Cursor::new(data);
        let s = Self {
            chunk_id:   read_buf.read_u32::<BigEndian>().unwrap(),
            chunk_size: read_buf.read_u32::<LittleEndian>().unwrap(),
            format:     read_buf.read_u32::<BigEndian>().unwrap(),
        };

        s.check();

        s
    }

    fn deserialize(&self, raw: &mut Vec<u8>) {
        raw.write_u32::<BigEndian>(self.chunk_id).unwrap();
        raw.write_u32::<LittleEndian>(self.chunk_size).unwrap();
        raw.write_u32::<BigEndian>(self.format).unwrap();
    }

    fn check(&self) {
        assert_eq!(self.chunk_id, 0x52494646); // RIFF
        assert_eq!(self.format, 0x57415645); // WAVE
    }
}

pub struct FmtSubchunk {
    pub subchunk_id:     u32, // big
    pub subchunk_size:   u32, // little
    pub audio_format:    u16, // little
    pub num_channels:    u16, // little
    pub sample_rate:     u32, // little
    pub byte_rate:       u32, // little
    pub block_align:     u16, // little
    pub bits_per_sample: u16, // little
}

impl FmtSubchunk {
    fn serialize(data: &[u8]) -> Self {
        let mut read_buf = Cursor::new(data);
        let s = Self {
            subchunk_id:     read_buf.read_u32::<BigEndian>().unwrap(),
            subchunk_size:   read_buf.read_u32::<LittleEndian>().unwrap(),
            audio_format:    read_buf.read_u16::<LittleEndian>().unwrap(),
            num_channels:    read_buf.read_u16::<LittleEndian>().unwrap(),
            sample_rate:     read_buf.read_u32::<LittleEndian>().unwrap(),
            byte_rate:       read_buf.read_u32::<LittleEndian>().unwrap(),
            block_align:     read_buf.read_u16::<LittleEndian>().unwrap(),
            bits_per_sample: read_buf.read_u16::<LittleEndian>().unwrap(),
        };

        s.check();

        s
    }

    fn deserialize(&self, raw: &mut Vec<u8>) {
        raw.write_u32::<BigEndian>(self.subchunk_id).unwrap();
        raw.write_u32::<LittleEndian>(self.subchunk_size).unwrap();
        raw.write_u16::<LittleEndian>(self.audio_format).unwrap();
        raw.write_u16::<LittleEndian>(self.num_channels).unwrap();
        raw.write_u32::<LittleEndian>(self.sample_rate).unwrap();
        raw.write_u32::<LittleEndian>(self.byte_rate).unwrap();
        raw.write_u16::<LittleEndian>(self.block_align).unwrap();
        raw.write_u16::<LittleEndian>(self.bits_per_sample).unwrap();
    }

    fn check(&self) {
        assert_eq!(self.subchunk_id, 0x666D7420); // "fmt "
        assert_eq!(
            self.byte_rate,
            self.sample_rate * self.num_channels as u32 * self.bits_per_sample as u32 / 8
        );
        assert_eq!(
            self.block_align,
            self.num_channels * self.bits_per_sample / 8
        );
        // TODO: let it work other than 16 bit
        assert_eq!(self.bits_per_sample, 16);
    }
}

pub struct DataSubchunk {
    pub subchunk_id:   u32,      // big
    pub subchunk_size: u32,      // little
    pub data:          Vec<u16>, // little
}

impl DataSubchunk {
    fn serialize(data: &[u8]) -> Self {
        let mut read_buf = Cursor::new(data);
        let s = Self {
            subchunk_id:   read_buf.read_u32::<BigEndian>().unwrap(),
            subchunk_size: read_buf.read_u32::<LittleEndian>().unwrap(),
            data:          {
                // TODO: preallocate memory
                let mut v = Vec::new();

                while read_buf.position() < data.len() as u64 {
                    v.push(read_buf.read_u16::<LittleEndian>().unwrap());
                }

                v
            },
        };

        s.check();

        s
    }

    fn deserialize(&self, raw: &mut Vec<u8>) {
        raw.write_u32::<BigEndian>(self.subchunk_id).unwrap();
        raw.write_u32::<LittleEndian>(self.subchunk_size).unwrap();
        for d in &self.data {
            raw.write_u16::<LittleEndian>(*d).unwrap()
        }
    }

    fn check(&self) {
        assert_eq!(self.subchunk_id, 0x64617461); // data
        assert_eq!(self.subchunk_size, self.data.len() as u32 * 2);
    }
}

#[cfg(test)]
mod tests {
    use std::{
        fs::{self, File},
        io::{Read, Write},
    };

    use super::Wave;

    #[test]
    fn read_write_corruption() {
        let mut read_buffer: Vec<u8> = Vec::new();

        File::open("test_file.wav")
            .unwrap()
            .read_to_end(&mut read_buffer)
            .unwrap();

        assert!(read_buffer.len() > 0);

        let serialized_file = Wave::serialize(&read_buffer);

        File::create("test_file_rewritten.wav")
            .unwrap()
            .write_all(&serialized_file.deserialize())
            .unwrap();

        // Clean
        fs::remove_file("./test_file_rewritten.wav").unwrap();
    }
}
