use std::path::PathBuf;

use clap::{Parser, Subcommand, ValueHint};

/// A tool for hidding a file into a Wave file and revor it again
#[derive(Parser)]
#[command(version)]
pub struct Cmd {
    /// Action to take
    #[command(subcommand)]
    pub sub_cmd: SubCmd,
}

#[derive(Subcommand)]
pub enum SubCmd {
    /// Hide some file into WAVE file
    Hide {
        /// The WAVE file which information will be stored in
        #[arg(short, long, value_hint = ValueHint::FilePath)]
        wave_file:    PathBuf,
        /// The file which contains the information that will be hidden
        #[arg(short, long, value_hint = ValueHint::FilePath)]
        file_to_hide: PathBuf,
        /// The name of the generated output file
        #[arg(short, long)]
        output:       PathBuf,
    },
    /// Recover the hidden file from WAVE file
    Recover {
        /// The WAVE file which information will be recovered from
        #[arg(short, long, value_hint = ValueHint::FilePath)]
        wave_file: PathBuf,
        /// The name of the generated output file
        #[arg(short, long)]
        output:    PathBuf,
    },
}
